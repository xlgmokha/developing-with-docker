#!/bin/sh

set -e

name="$1"
image_id="$(docker image ls "$name" | awk '{ print $3 }' | grep -v REPO | tail -n1)"
echo "$ dive $image_id"
dive "$image_id"
