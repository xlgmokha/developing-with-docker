#!/bin/sh

set -e

image_id="$(docker ps | grep developing | awk '{ print $1 }' | tail -n1)"
docker exec -it "$image_id" /bin/sh
