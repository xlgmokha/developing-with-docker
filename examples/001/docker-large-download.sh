#!/bin/sh

docker rmi -f "$(docker images | grep 'license-management')" 2> /dev/null
time docker pull registry.gitlab.com/gitlab-org/security-products/license-management:latest
